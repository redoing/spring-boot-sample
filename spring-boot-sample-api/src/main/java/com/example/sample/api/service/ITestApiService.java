package com.example.sample.api.service;

import org.springframework.stereotype.Service;

/**
 * TestApiService interface
 *
 * @author zhangl
 * @date 2021/10/23 19:17
 */
@Service
public interface ITestApiService {
    /**
     * 获取一个字符串
     * @return 字符串
     */
    String getTestContent();
}
