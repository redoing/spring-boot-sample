package com.example.sample.api.service.impl;

import com.example.sample.api.service.ITestApiService;
import org.springframework.stereotype.Service;

/**
 * TestApiServiceImpl class
 *
 * @author zhangl
 * @date 2021/10/23 19:18
 */
@Service(value = "ITestApiService")
public class TestApiServiceImpl implements ITestApiService {
    @Override
    public String getTestContent() {
        return "TestApiServiceImpl Content ... success ";
    }
}
