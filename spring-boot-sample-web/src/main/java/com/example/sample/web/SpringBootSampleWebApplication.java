package com.example.sample.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *启动类
 * //@ComponentScan 解决跨模块调用 bean not found的问题<br>
 *     扫描 com.example.sample 包
 * @author zhangl
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.example.sample"})
public class SpringBootSampleWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSampleWebApplication.class, args);
    }

}
