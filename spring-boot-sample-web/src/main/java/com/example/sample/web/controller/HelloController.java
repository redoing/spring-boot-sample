package com.example.sample.web.controller;

import com.example.sample.api.service.ITestApiService;
import com.example.sample.web.service.IHelloService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * HelloController class
 *
 * @author zhangl
 * @date 2021/10/23 19:06
 */
@RestController
public class HelloController {
    @Resource
    private IHelloService helloService ;
    /**
     * 注入api模块的ITestApiService
     */
    @Resource
    private ITestApiService testApiService;

    @GetMapping("/hello")
    public String hello(){
        return "Hello World ! " + helloService.sayHello() + testApiService.getTestContent();
    }
    @GetMapping("/sayHello")
    public String sayHello(){
        return helloService.sayHello();
    }
    @GetMapping("/success")
    public String getTestContent(){
        return testApiService.getTestContent();
    }

}
