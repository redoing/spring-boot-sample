package com.example.sample.web.service;

import org.springframework.stereotype.Service;

/**
 * IHelloService class
 *
 * @author zhangl
 * @date 2021/10/23 19:10
 */
@Service
public interface IHelloService {
    /**
     * sayHello ，
     * @return 字符串
     */
    String sayHello();
}
