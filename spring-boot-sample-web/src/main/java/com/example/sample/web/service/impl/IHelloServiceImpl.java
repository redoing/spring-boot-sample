package com.example.sample.web.service.impl;

import com.example.sample.web.service.IHelloService;
import org.springframework.stereotype.Service;

/**
 * IHelloServiceImpl class
 *
 * @author zhangl
 * @date 2021/10/23 19:11
 */
@Service(value = "iHelloService")
public class IHelloServiceImpl implements IHelloService {
    @Override
    public String sayHello() {
        return "IHelloServiceImpl sayHello Method ... ";
    }
}
